# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vit Pelcak <vit@pelcak.org>, 2012, 2018, 2021.
# Lukáš Tinkl <lukas@kde.org>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-15 00:20+0000\n"
"PO-Revision-Date: 2021-03-23 11:33+0100\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.3\n"
"X-Language: cs_CZ\n"
"X-Source-Language: en_US\n"

#: multiplexedservice.cpp:68
#, kde-format
msgctxt "Name for global shortcuts category"
msgid "Media Controller"
msgstr "Ovládání médií"

#: multiplexedservice.cpp:70
#, kde-format
msgid "Play/Pause media playback"
msgstr "Přehrát/pozastavit média"

#: multiplexedservice.cpp:88
#, kde-format
msgid "Media playback next"
msgstr "Další média"

#: multiplexedservice.cpp:97
#, kde-format
msgid "Media playback previous"
msgstr "Předchozí média"

#: multiplexedservice.cpp:106
#, kde-format
msgid "Stop media playback"
msgstr "Zastavit přehrávání média"

#: multiplexedservice.cpp:115
#, kde-format
msgid "Pause media playback"
msgstr "Pozastavit přehrávání média"

#: multiplexedservice.cpp:124
#, kde-format
msgid "Play media playback"
msgstr "Přehrát médium"

#: multiplexedservice.cpp:133
#, kde-format
msgid "Media volume up"
msgstr "Zesílení hlasitosti"

#: multiplexedservice.cpp:142
#, kde-format
msgid "Media volume down"
msgstr "Zeslabení hlasitosti"

#: playeractionjob.cpp:169
#, kde-format
msgid "The media player '%1' cannot perform the action '%2'."
msgstr "Přehrávač médií '%1' nemůže provést činnost '%2'."

#: playeractionjob.cpp:171
#, kde-format
msgid "Attempting to perform the action '%1' failed with the message '%2'."
msgstr "Pokus o provedení akce '%1' selhal se zprávou '%2'."

#: playeractionjob.cpp:173
#, kde-format
msgid "The argument '%1' for the action '%2' is missing or of the wrong type."
msgstr "Argument '%1' pro akci '%2' chybí nebo je nesprávného typu."

#: playeractionjob.cpp:175
#, kde-format
msgid "The operation '%1' is unknown."
msgstr "Operace '%1' není známa."

#: playeractionjob.cpp:177
#, kde-format
msgid "Unknown error."
msgstr "Neznámá chyba."
